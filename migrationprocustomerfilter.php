<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from MigrationPro
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@migration-pro.com
 *
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe MigrationPro
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la MigrationPro est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la MigrationPro a l'adresse: contact@migration-pro.com
 *
 * @author    MigrationPro
 * @copyright Copyright (c) 2012-2019 MigrationPro
 * @license   Commercial license
 * @package   MigrationPro: MigrationPro Customer Filter
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Migrationprocustomerfilter extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'migrationprocustomerfilter';
        $this->tab = 'search_filter';
        $this->version = '1.0.0';
        $this->author = 'MIgrationPro';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('MigrationPro Customer Filter');
        $this->description = $this->l('MigrationPro Customer search by ordered products');

        $this->confirmUninstall = $this->l('successful uninstall the module');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('MIGRATIONPROCUSTOMERFILTER_LIVE_MODE', false);
        Configuration::updateValue('migrationprocustomerfilter_module_path', $this->local_path);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayAdminStatsModules');
    }

    public function uninstall()
    {
        Configuration::deleteByName('MIGRATIONPROCUSTOMERFILTER_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitMigrationprocustomerfilterModule')) == true) {
            $this->postProcess();
        }


//        if (((bool)Tools::isSubmit('btn-assign')) == true) {
//            $aa = self::renderCustomer();
//            return (Tools::jsonEncode($aa));
//        }

        $id_def_lang = Configuration::get('PS_LANG_DEFAULT');
        $products = Db::getInstance()->executes('SELECT p.id_product, pl.name FROM `' . _DB_PREFIX_ . 'product` p inner JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON p.id_product = pl.id_product WHERE id_lang = ' . $id_def_lang);
        $customer_groups = Group::getGroups($id_def_lang);
        $this->context->smarty->assign('customer_groups_reasign', $customer_groups);
        $this->context->smarty->assign('module_dir', $this->_path);
        $this->context->smarty->assign('products', $products);
        $this->context->smarty->assign('validate_url', $this->context->link->getAdminLink('AdminMigrationProCustomerFilter'));


        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

        return $output;
    }


    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitMigrationprocustomerfilterModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'MIGRATIONPROCUSTOMERFILTER_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'MIGRATIONPROCUSTOMERFILTER_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'MIGRATIONPROCUSTOMERFILTER_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'MIGRATIONPROCUSTOMERFILTER_LIVE_MODE' => Configuration::get('MIGRATIONPROCUSTOMERFILTER_LIVE_MODE', true),
            'MIGRATIONPROCUSTOMERFILTER_ACCOUNT_EMAIL' => Configuration::get('MIGRATIONPROCUSTOMERFILTER_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'MIGRATIONPROCUSTOMERFILTER_ACCOUNT_PASSWORD' => Configuration::get('MIGRATIONPROCUSTOMERFILTER_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
//        if (Tools::getValue('module_name') == $this->name) {
        $this->context->controller->addJS($this->_path . 'views/js/back.js');
        $this->context->controller->addCSS($this->_path . 'views/css/back.css');
//        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        Media::addJsDef(array(
            'teztez' => $this->_path
        ));
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');
    }

    public function hookDisplayAdminStatsModules()
    {
        /* Place your code here. */
    }
}
