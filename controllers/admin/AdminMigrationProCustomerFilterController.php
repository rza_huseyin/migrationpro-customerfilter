<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from MigrationPro
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@migration-pro.com
 *
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe MigrationPro
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la MigrationPro est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la MigrationPro a l'adresse: contact@migration-pro.com
 *
 * @author    MigrationPro
 * @copyright Copyright (c) 2012-2019 MigrationPro
 * @license   Commercial license
 * @package   MigrationPro: MigrationPro Customer Filter
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class AdminMigrationProCustomerFilterController extends AdminController
{
    // --- response vars:
    protected $response;

    public function __construct()
    {
        $this->display = 'edit';
        parent::__construct();
        $this->controller_type = 'moduleadmin'; //instead of AdminController’s admin
        $tab = new Tab($this->id); // an instance with your tab is created; if the tab is not attached to the module, the exception will be thrown
        if (!$tab->module) {
            throw new PrestaShopException('Admin tab ' . get_class($this) . ' is not a module tab');
        }
        $this->module = Module::getInstanceByName($tab->module);
        if (!$this->module->id) {
            throw new PrestaShopException("Module {$tab->module} not found");
        }
        $this->tabAccess = Profile::getProfileAccess($this->context->employee->id_profile, Tab::getIdFromClassName('AdminMigrationPro'));
    }


   /*
    * ajax methods
    * */
    public function ajaxProcessSearch()
    {
        $product_ids = Tools::getValue('products');
        $search_type_and = Tools::getValue('search_type_and');
        $search_type_or = Tools::getValue('search_type_or');
        if ($product_ids == "  " ||( $search_type_and == 'undefined' && $search_type_or == 'undefined')) {
            $this->response['success'] = $this->context->smarty->fetch(Configuration::get('migrationprocustomerfilter_module_path') . 'views/templates/admin/_warning.tpl');
            die(Tools::jsonEncode($this->response));
        }
        if (empty($product_ids)) {
            $this->response['success'] = $this->context->smarty->fetch(Configuration::get('migrationprocustomerfilter_module_path') . 'views/templates/admin/_warning.tpl');
            die(Tools::jsonEncode($this->response));
        }
        if ($search_type_or == 'on' && $search_type_and != 'on') {
            $customers = Db::getInstance()->executes('SELECT c.id_customer, c.firstname, c.lastname, c.email 
                                                  FROM `' . _DB_PREFIX_ . 'customer` c 
                                                    INNER JOIN ' . _DB_PREFIX_ . 'orders o ON  c.id_customer = o.id_customer
                                                    INNER JOIN  ' . _DB_PREFIX_ . 'order_detail od ON od.id_order = o.id_order 
                                                    WHERE od.product_id in (' . $product_ids . ') GROUP BY c.id_customer');
        } elseif ($search_type_and == 'on') {
            $customer_info = Db::getInstance()->executes('SELECT c.id_customer, c.firstname, c.lastname, c.email , od.product_id
                                                  FROM `' . _DB_PREFIX_ . 'customer` c 
                                                    INNER JOIN ' . _DB_PREFIX_ . 'orders o ON  c.id_customer = o.id_customer
                                                    INNER JOIN  ' . _DB_PREFIX_ . 'order_detail od ON od.id_order = o.id_order 
                                                    WHERE od.product_id in (' . $product_ids . ')  GROUP BY c.id_customer, od.product_id');
            $customer = array();
            foreach ($customer_info as $ci) {
                $customer[$ci['id_customer']][$ci['product_id']] = $ci['product_id'];
            }
            $product_id_arr = explode(',', str_replace(' ', '', $product_ids));
            $customer_id = array();
            foreach ($customer as $key=>$value) {
                $result = array_diff($product_id_arr, $value);
                if (empty($result)) {
                        $customer_id[] = $key;
                }
            }
            $customers = Db::getInstance()->executes('SELECT c.id_customer, c.firstname, c.lastname, c.email 
                                                  FROM `' . _DB_PREFIX_ . 'customer` c WHERE c.id_customer in ( ' . implode(",", $customer_id) . ')');
        } else {
            die;
        }


        $id_def_lang = Configuration::get('PS_LANG_DEFAULT');
        $customer_groups = Group::getGroups($id_def_lang);
        $this->context->smarty->assign('customers', $customers);
        $this->context->smarty->assign('customer_groups', $customer_groups);
        $this->response['customer'] = $this->context->smarty->fetch(Configuration::get('migrationprocustomerfilter_module_path') . 'views/templates/admin/_customer.tpl');

        die(Tools::jsonEncode($this->response));
    }

    public function ajaxProcessAssign()
    {
        $customer_ids = Tools::getValue('customers');
        $customergroup_id = Tools::getValue('customergroup_id');
        $query = '(' . str_replace(',', ',' . $customergroup_id . '), (', $customer_ids) . ', ' . $customergroup_id . ')';
        $res = Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'customer_group WHERE id_customer in(' . $customer_ids . ')');
        $res = Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'customer SET id_default_group = '. $customergroup_id . ' WHERE id_customer in(' . $customer_ids . ')');

        $res = Db::getInstance()->execute('REPLACE INTO ' . _DB_PREFIX_ . 'customer_group VALUE ' . $query);

        if ($res) {
            $this->response['success'] = $this->context->smarty->fetch(Configuration::get('migrationprocustomerfilter_module_path') . 'views/templates/admin/_success.tpl');
        } else {
            $error = Db::getInstance()->getMsgError();
            $this->content->smarty->assing('error', $error);
            $this->response['success'] = $this->context->smarty->fetch(Configuration::get('migrationprocustomerfilter_module_path') . 'views/templates/admin/_warning.tpl');
        }

        die(Tools::jsonEncode($this->response));
    }

    public function ajaxProcessReassignSearch()
    {
        $id_def_lang = Configuration::get('PS_LANG_DEFAULT');
        $customergroup_id = Tools::getValue('customergroup_id');
        $customer_groups = Group::getGroups($id_def_lang);

        $customers = Db::getInstance()->executes('SELECT c.id_customer, c.firstname, c.lastname, c.email FROM ' . _DB_PREFIX_ . 'customer_group cg INNER JOIN ' . _DB_PREFIX_ . 'customer c ON c.id_customer = cg.id_customer WHERE cg.id_group =  ' . $customergroup_id);

        $this->context->smarty->assign('customer_groups_reasign', $customer_groups);
        $this->context->smarty->assign('customers', $customers);
        $this->response['customer'] = $this->context->smarty->fetch(Configuration::get('migrationprocustomerfilter_module_path') . 'views/templates/admin/_customerReasign.tpl');

        die(Tools::jsonEncode($this->response));
    }

    /**
     * reassigning customers function.
     */
    public function ajaxProcessReassign()
    {
        //selected group ID
        $id_def_lang = Configuration::get('PS_LANG_DEFAULT');
        $customergroup_id = Tools::getValue('customergroup_id');
        $customergroupback_id = Tools::getValue('customergroupback_id');
        $customer_ids = Tools::getValue('customers');

        $query = '(' . str_replace(',', ',' . $customergroupback_id . '), (', $customer_ids) . ', ' . $customergroupback_id . ')';

//        selected customers IDs
        if (empty($customergroup_id) || empty($customer_ids) || empty($customergroupback_id)) {
            die;
        }
        $res = Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'customer SET id_default_group = '. $customergroupback_id . ' WHERE id_customer in (' . $customer_ids . ')');
//        $res = Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'customer_group SET id_group = '. $customergroupback_id . ' WHERE id_customer in (' . $customer_ids . ')');
//        die('REPLACE INTO ' . _DB_PREFIX_ . 'customer_group VALUE ' . $query);
//        $res = Db::getInstance()->execute('REPLACE INTO ' . _DB_PREFIX_ . 'customer_group VALUE ' . $query);
        $res = Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'customer_group WHERE id_customer in(' . $customer_ids . ') AND  id_group =  ' . $customergroup_id);
        $res = Db::getInstance()->execute('REPLACE INTO ' . _DB_PREFIX_ . 'customer_group VALUE ' . $query);
//
        $customers = Db::getInstance()->executes('SELECT c.id_customer, c.firstname, c.lastname, c.email FROM ' . _DB_PREFIX_ . 'customer_group cg INNER JOIN ' . _DB_PREFIX_ . 'customer c ON c.id_customer = cg.id_customer WHERE cg.id_group =  ' . $customergroup_id);
        $customer_groups = Group::getGroups($id_def_lang);

        $this->context->smarty->assign('customer_groups_reasign', $customer_groups);
        $this->context->smarty->assign('customers', $customers);
        $this->response['customer'] = $this->context->smarty->fetch(Configuration::get('migrationprocustomerfilter_module_path') . 'views/templates/admin/_customerReasign.tpl');
        $this->response['success'] = $this->context->smarty->fetch(Configuration::get('migrationprocustomerfilter_module_path') . 'views/templates/admin/_success_reassign.tpl');
        die(Tools::jsonEncode($this->response));
    }

}
