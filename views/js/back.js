/**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2019 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */


$(document).ready(function () {

    // ---START---
    // click checkbox when click a row of table
    $("#product_list").on('click', 'td', function () {
        var currentRow = $(this).closest("tr");
        if (currentRow.find(".form-check-input").is(':checked')) {
            currentRow.find(".form-check-input").prop("checked", false);
        } else {
            currentRow.find(".form-check-input").prop("checked", true);
        }
    });

    function checkbox_click_customer() {
        $("#customer_list").on('click', 'td', function () {
            var currentRow = $(this).closest("tr");
            if (currentRow.find(".form-check-input").is(':checked')) {
                currentRow.find(".form-check-input").prop("checked", false);
            } else {
                currentRow.find(".form-check-input").prop("checked", true);
            }
        });
    }
    function checkbox_click_customer_reassing() {
        $("#customer_list_reassign").on('click', 'td', function () {
            var currentRow = $(this).closest("tr");
            if (currentRow.find(".form-check-input").is(':checked')) {
                currentRow.find(".form-check-input").prop("checked", false);
            } else {
                currentRow.find(".form-check-input").prop("checked", true);
            }
        });
    }

    // -----END ----

    // select all checkbox
    function select_all_customer(){
        $("#btn-selectall").click( function() {
            $("#customer_list").find(".form-check-input").attr('checked', true);
            // return false;
        });
    }
    // select all checkbox
    function select_all_customer_reassign(){
        $("#btn-selectall-reassign").click( function() {
            $("#customer_list_reassign").find(".form-check-input").attr('checked', true);
            // return false;
        });
    }


    $('#btn_search').click(function () {
        $('#alert-success').empty();
        $.ajax({
            type: "POST",
            url: validate_url,
            async: false,
            dataType: 'json',
            data: 'products=' + get_products_id() + '&search_type_and=' + get_search_type_and() + '&search_type_or=' + get_search_type_or() + '&action=search&ajax=1',
            success: function (datas) {
                $("#customers_mod").empty().append(datas.customer);
                $("#alert-success_product").empty().append(datas.success);
                //called bellow function for asign apend element
                assign();
                export_();
                checkbox_click_customer();
                select_all_customer();

                // if (datas.has_error || datas.has_warning) {
                //
                //     if (datas.has_error)
                //         displayError(datas.errors, context.fromStep);
                //
                //     if (datas.has_warning)
                //         displayWarning(datas.warnings, context.fromStep);
                //
                //     resizeWizard();
                // }
                // else {
                //     displayForm(datas.step_form, context.fromStep);
                //     importData();
                // }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                if (XMLHttpRequest.responseText.length > 0)
                    $('#alert-success').prepend(XMLHttpRequest.responseText);
            }
        });
    });

    function assign() {
        $('#btn_assign').click(function () {
            $("#alert-success").empty();
            $.ajax({
                type: "POST",
                url: validate_url,
                async: false,
                dataType: 'json',
                data: 'customers= ' + get_customers_id() + ' &customergroup_id=' + get_customergroup() + '&action=assign&ajax=1',
                success: function (datas) {
                    $("#alert-success").empty().append(datas.success);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    if (XMLHttpRequest.responseText.length > 0)
                        $('#alert-success').prepend(XMLHttpRequest.responseText);
                }
            });
        });
    }

    function reassign() {
        $('#btn_reassign').click(function () {
            $("#alert-success_reassign").empty();
            $.ajax({
                type: "POST",
                url: validate_url,
                async: false,
                dataType: 'json',
                data: 'customers=' + get_customers_id_reassign() + '&customergroup_id=' + get_customergroup_reassign() + '&customergroupback_id=' + get_customergroup_reassign_back() +'&action=reassign&ajax=1',
                success: function (datas) {
                    $("#alert-success_reassign").empty().append(datas.success);
                    $("#customers_mod_reassigned").empty().append(datas.customer);
                    checkbox_click_customer_reassing();
                    reassign();
                    select_all_customer_reassign();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    if (XMLHttpRequest.responseText.length > 0)
                        $('#alert-success_reassign').prepend(XMLHttpRequest.responseText);
                }
            });
        });
    }


    $('#changle_search_reassign').click(function () {
        $('#alert-success').empty();
        $.ajax({
            type: "POST",
            url: validate_url,
            async: false,
            dataType: 'json',
            data: 'customergroup_id=' + get_customergroup_reassign() + '&action=reassign_search&ajax=1',
            success: function (datas) {
                $("#customers_mod_reassigned").empty().append(datas.customer);
                checkbox_click_customer_reassing();
                reassign();
                select_all_customer_reassign();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                if (XMLHttpRequest.responseText.length > 0)
                    $('#alert-success').empty().prepend(XMLHttpRequest.responseText);
            }
        });
    });


    function export_() {
        $('#btn_export').click(function () {
            download_csv();
        });
    }


    // get selectect products ID
    function get_products_id() {
        var favorite = [];
        $.each($("input[name='products']:checked"), function () {
            favorite.push($(this).val());
        });
        return favorite.join(", ");
    }

    // get selectect search type     : AND OR
    function get_search_type_and() {
        return $('input[name=and_or_radio_and]:checked').val();
    }
    // get selectect search type     : AND OR
    function get_search_type_or() {
        return $('input[name=and_or_radio_or]:checked').val();
    }

    // get selectect customers ID
    function get_customers_id() {
        var favorite = [];
        $.each($("input[name='customers']:checked"), function () {
            favorite.push($(this).val());
        });
        return favorite.join(", ");
    }

    // get selectect customers REASSIGN ID
    function get_customers_id_reassign() {
        var favorite = [];
        $.each($("input[name='customers_reassign']:checked"), function () {
            favorite.push($(this).val());
        });
        return favorite.join(", ");
    }

    // get selectect customers information
    function get_customers_info() {
        var favorite = [];
        $.each($("input[name='customers']:checked"), function () {
            favorite.push($(this).parent().next().find("label").text().split(","));
        });
        return favorite;
    }

    // get selectect customer group ID
    function get_customergroup() {
        return $('#customer_group_options').find(":selected").val();
    }

    // get selectect customer group ID reassigned
    function get_customergroup_reassign() {
        return $('#customer_group_options_reasign').find(":selected").val();
    }

    // get selectect customer group ID reassigned back
    function get_customergroup_reassign_back() {
        return $('#customer_group_options_reasign_back').find(":selected").val();
    }

    //CSV
    var data = [
        ['Foo', 'programmer', 'programmer'],
        ['Bar', 'bus driver', 'bus driver'],
        ['Moo', 'Reindeer Hunter', 'Reindeer Hunter']
    ];


    function download_csv() {
        var customer_info = get_customers_info();
        var csv = 'ID, Name,Surname, Email\n';
        customer_info.forEach(function (row) {
            csv += row.join(',');
            csv += "\n";
        });

        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_blank';
        hiddenElement.download = 'people.csv';
        hiddenElement.click();
    }


})

