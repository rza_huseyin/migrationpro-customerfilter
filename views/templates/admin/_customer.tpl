{*
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from MigrationPro
* Use, copy, modification or distribution of this source file without written
* license agreement from the MigrationPro is strictly forbidden.
* In order to obtain a license, please contact us: contact@migration-pro.com
*
* INFORMATION SUR LA LICENCE D'UTILISATION
*
* L'utilisation de ce fichier source est soumise a une licence commerciale
* concedee par la societe MigrationPro
* Toute utilisation, reproduction, modification ou distribution du present
* fichier source sans contrat de licence ecrit de la part de la MigrationPro est
* expressement interdite.
* Pour obtenir une licence, veuillez contacter la MigrationPro a l'adresse: contact@migration-pro.com
*
* @author    MigrationPro
* @copyright Copyright (c) 2012-2019 MigrationPro
* @license   Commercial license
* @package   MigrationPro: MigrationPro Customer Filter
*}

{*customer list which ordered selected products*}

<hr>
{block name='customer_pack'}
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
            <h1 class="font-weight-bold">Customers List</h1>
        </div>
        <div class="col-md-7">
            <button type="button" class="btn btn-primary" id="btn-selectall" name="btn-selectall">Select All</button>
        </div>
        <div class="col-md-7">
            <div class="product-pack ">
                <table id="customer_list" class="table table-striped">
                    {foreach from=$customers item="customer"}
                        {block name='custoemr_miniature'}
                            <tr>
                                <td>
                                    <input type="checkbox" class="form-check-input" value="{$customer.id_customer}"
                                           name="customers">
                                </td>
                                <td style="width:100%;" class="customer_info">
                            <label class="form-check-label"
   for="exampleCheck1">{$customer.id_customer}, {$customer.firstname}, {$customer.lastname}, {$customer.email}</label>
                                </td>
                            </tr>
                        {/block}
                    {/foreach}
                </table>
            </div>
        </div>
    </div>
    <br>
    <br>
    {*<div  class="col-md-12"></div>*}
    <div id="alert-success_export" class="col-md-10">
    </div>
    <div class="col-md-9">

    </div>
    <div style="align-self:center" class="col-md-2">
        <select class="form-control" id="customer_group_options">
            {foreach from=$customer_groups item="customer_group"}
                <option value="{$customer_group.id_group}">{$customer_group.id_group} - {$customer_group.name}</option>
            {/foreach}
        </select>
        <br>
        <br>
        <div class="changle_assign">
            <button type="button" class="btn btn-success" id="btn_export" name="btn-export">CSV Export</button>
            <button type="button" class="btn btn-success" id="btn_assign" name="btn-assign">ASSIGN
            </button>
        </div>

    </div>
    <br>
{/block}


