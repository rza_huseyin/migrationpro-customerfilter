{*
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from MigrationPro
* Use, copy, modification or distribution of this source file without written
* license agreement from the MigrationPro is strictly forbidden.
* In order to obtain a license, please contact us: contact@migration-pro.com
*
* INFORMATION SUR LA LICENCE D'UTILISATION
*
* L'utilisation de ce fichier source est soumise a une licence commerciale
* concedee par la societe MigrationPro
* Toute utilisation, reproduction, modification ou distribution du present
* fichier source sans contrat de licence ecrit de la part de la MigrationPro est
* expressement interdite.
* Pour obtenir une licence, veuillez contacter la MigrationPro a l'adresse: contact@migration-pro.com
*
* @author    MigrationPro
* @copyright Copyright (c) 2012-2019 MigrationPro
* @license   Commercial license
* @package   MigrationPro: MigrationPro Customer Filter
*}

<script>
    var validate_url = '{$validate_url|addslashes|escape:'javascript':'UTF-8'}';
</script>
<div class="panel">
    <div class="row moduleconfig-header">
        {*<div class="col-xs-5 text-right">*}
        {*<img src="{$module_dir|escape:'html':'UTF-8'}views/img/logo.jpg" />*}
        {*</div>*}
        <div class="col-xs-7 text-left">
            <h2>{l s='Migrationpro Customer Filter' mod='migrationprocustomerfilter'}</h2>
            <h4>{l s='Grouped customers by ordered products' mod='migrationprocustomerfilter'}</h4>
        </div>
    </div>

    <hr/>
    <div class="moduleconfig-content">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">

                    <div class="product-actions">

                        {block name='product_list'}
                            {block name='product_pack'}
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-3">
                                    <h1 class="font-weight-bold">Products List <span
                                                class="badge badge-secondary">All</span></h1>
                                </div>
                                <div class="col-md-7">
                                    <div class="product-pack ">
                                        <table id="product_list" class="table table-striped">
                                            {foreach from=$products item="product_name"}
                                                {block name='product_miniature'}
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" class="form-check-input"
                                                                   value="{$product_name.id_product}" name="products">
                                                        </td>
                                                        <td style="width:100%;">
                                                            <label class="form-check-label"
                                                                   for="exampleCheck1">{$product_name.name}</label>
                                                        </td>
                                                    </tr>
                                                {/block}
                                            {/foreach}
                                        </table>
                                    </div>
                                </div>
                            {/block}
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-5">
                                        <div class="col-md-1">
                                            <div class="radio">
                                                <input type="checkbox" class="form-check-input" name="and_or_radio_and">
                                                <label style="padding-left: 0px">AND</label>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="radio">
                                                <input type="checkbox" class="form-check-input"  name="and_or_radio_or">
                                                <label style="padding-left: 0px">OR</label>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <button type="button" id="btn_search" class="btn btn-primary">Search
                                            </button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="alert-success_product" class="col-md-10">
                                </div>
                            </div>
                            <br>
                            <br>
                            {block name='customer_pack'}
                                <div class="customer-pack col-md-12" id="customers_mod">
                                    {*customer list which ordered selected products*}
                                </div>
                            {/block}
                        {/block}

                    </div>
                    <div class="col-md-1"></div>
                    <div id="alert-success" class="col-md-10">
                    </div>
                </div>
            </div>
            <hr>
            <div class="col-md-12">
                <div class="row">
                    <hr>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <h1 class="font-weight-bold">Customer Group <span class="badge badge-secondary">Reassign</span>
                        </h1>
                    </div>
                    <div class="col-md-7">
                        <div style="align-self:center" class="col-md-2">
                            <select class="form-control" id="customer_group_options_reasign">
                                {foreach from=$customer_groups_reasign item="customer_group_reasign"}
                                    <option value="{$customer_group_reasign.id_group}">{$customer_group_reasign.id_group}
                                        - {$customer_group_reasign.name}</option>
                                {/foreach}
                            </select>
                            <br>
                            <br>
                            <div class="changle_search_reassign" style="float: right">
                                <button type="button" class="btn btn-success" id="changle_search_reassign"
                                        name="changle-search-reassign">Search
                                </button>
                            </div>
                            {*<br>*}
                            {*<div class="changle_reassign">*}
                            {*<button type="button" class="btn btn-success" id="btn_reassign" name="btn-reassign">Reassign*}
                            {*</button>*}
                            {*</div>*}
                        </div>
                        <div class="col-md-10">
                            <div class="customer-pack col-md-12" id="customers_mod_reassigned">
                                {*customer list which ordered selected products*}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" id="alert-success_reassign"></div>
                </div>
            </div>
            <br/>

            <p class="text-center">
                <strong>
                    <a href="http://www.prestashop.com" target="_blank" title="Lorem ipsum dolor">
                        {l s='MigrationPro Customer Filter' mod='migrationprocustomerfilter' }
                    </a>
                </strong>
            </p>
        </div>
    </div>
</div>

